for k,robot in pairs(data.raw["logistic-robot"]) do
	robot.speed = robot.speed * settings.startup["logistic-robot-speed"].value
end
for k,robot in pairs(data.raw["construction-robot"]) do
	robot.speed = robot.speed * settings.startup["construction-robot-speed"].value
end